const router = require('express').Router()
const path = require('path')
const playerFunctions = require(path.join(__dirname,'..','services','playerfunctions'))

router.get('/', async(req, res) => {
  // fetch the players datas
  const allPlayers = await playerFunctions.getAllPlayers().catch(err => console.log(err))
  return (!!allPlayers)? res.status(200).json(allPlayers) : res.status(500).end('Internal server Error') 
})

router.get('/:id', async(req, res) => {
  const tennisPlayerId = parseInt(req.params.id)

  // the id has to be a number
  if(!Number.isInteger(tennisPlayerId)) return res.status(400).end('Wrong format for the id - The id must be a number')
  
  // fetch the players datas
  const allPlayers = await playerFunctions.getAllPlayers().catch(err => console.log(err))

  // if no data are received --> error
  if(!allPlayers) return res.status(500).end('Internal server Error')

  // selection of the specific player
  const selectedPlayer = allPlayers.players.filter(player => player.id === tennisPlayerId)

  // if no player matches the id --> error 404
  if(selectedPlayer.length == 0) return res.status(404).end('No player found')

  //Here, everything is good
  return res.status(200).json(selectedPlayer)
})

module.exports = router
