const assert = require('chai').assert;
const path = require('path')
const playerFunctions = require(path.join(__dirname,'..','services','playerfunctions'))

describe('fetching players data with API', function() {
  it('should return players data', async function() {
    const result = await playerFunctions.getAllPlayers().catch(err => console.log(err))
    assert.isObject(result)
    assert.isArray(result.players)
    assert.isNotEmpty(result.players)
  })
});
