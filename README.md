## Description

  This API has been created for a technical test.
  
  This API retrieves data from an other API in order to show a few tennis players.

## gitLab

  This project is public.
  
  git repository:

  - https://gitlab.com/gary003/tennisplayerapi

## Technical Univers
  
  - Languages : NodeJS , expressJS
    
  - Tests framework : Mocha

## Dependances

  Those have to be install on your machine :

    - NPM and nodeJS ( https://nodejs.org/fr/ )
    - Git 

    - Docker(not mandatory)

## API installation

  Use git:

    `git clone https://gitlab.com/gary003/tennisplayerapi.git`

## Quick Start

  After the installation, In a shell:

    `cd tennisplayerApi`
    `npm i`
    `npm start`

  Or using docker(still in a shell):

    `cd tennisplayerApi`
    `docker image build -t tennisApi .`
    `docker run -d -p 3000:3000 tennisApi`

  In Your browser type this URL : 'localhost:3000/players' or 'localhost:3000/players/52'

  This should list you all the players available in the database.

  Or, you can use the online version (see 'cloud use' down below).

## Cloud use

  This API is on the cloud (web) and linked to the master branch of the github repository,
  any changes on this branch will be deploy on a server (CI/CD).

  try these URLs in a browser : 
   -  http://46.101.231.29:3000/players/
   -  http://46.101.231.29:3000/players/52

## Tests

  In a SHELL, at the root of the project, do:
    
    `npm run test`

## People

  Developer : Gary Johnson <gary.johnson.freelance@gmail.com>

## License

  [MIT](LICENSE)
