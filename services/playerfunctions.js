const fs = require('fs-extra')
const path = require('path')
const axios = require('axios')
const conf = require(path.join(__dirname,'..','config','config'))

module.exports.getAllPlayers = async() => {
  /* step 1 use of the json file
   const jsonFile = fs.readFileSync(path.join(__dirname,'..','/assets/headtohead.json'))
  */


  // step 3 fetching the API with axios
  const jsonFile = await axios.get(conf.APIString)
                              .catch(err => console.log(err))
                        
  // In Case the API fecth fails
  if(jsonFile.isAxiosError) return Promise.reject()

  // Players must be sort by id
  jsonFile.data.players = jsonFile.data.players.sort((player1,player2) => player1.id - player2.id)

  return jsonFile.data
}