const express = require('express')
const morgan = require('morgan')
const path = require('path')
const endPointsList = require('express-list-endpoints')
const playersRoute = require(path.join(__dirname, 'routes','players'))

const app = express()

app.use(morgan('common'))
app.use('/players', playersRoute)

app.get('/', (req, res) => {
  return res.status(200).send(endPointsList(app))
})

module.exports = app

