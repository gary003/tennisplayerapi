FROM node:10

WORKDIR /root/tennisplayerapi

COPY . .

RUN npm i

EXPOSE 3000

CMD ["npm","start]
