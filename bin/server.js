const app = require('../app')
const http = require('http')
const path = require('path')
const conf = require(path.join(__dirname,'..','config','config'))

const port = process.env.PORT || conf.port || '8080'

const server = http.createServer(app)

server.on('listening', () => console.log(`server listening on port : ${port}.`))

server.on('error', (error) => {
  if (error.syscall !== 'listen') throw error
  console.error(error)
  process.exit(1)
})

server.listen(port)
